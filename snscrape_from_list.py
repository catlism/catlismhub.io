# adapted from the following sources
# https://www.pythonfixing.com/2022/06/fixed-python-how-to-iterate-through.html
# https://github.com/MartinBeckUT/TwitterScraper/blob/127b15b3878ab0c1b74438011056d89152701db1/snscrape/python-wrapper/snscrape-python-wrapper.py
# https://github.com/satomlins/snscrape-by-location/blob/1f605fb6e1caff3577198792a7717ffbf3c3f454/snscrape_by_location_tutorial.ipynb

import snscrape.modules.twitter as sntwitter
import pandas as pd
import argparse

parser = argparse.ArgumentParser(formatter_class = argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("searchlist")
parser.add_argument('--max', dest = 'maxResults', type = lambda x: int(x) if int(x) >= 0 else parser.error('--max-results N must be zero or positive'), metavar = 'N', help = 'Only return the first N results')
args = parser.parse_args()
maxResults = args.maxResults
search_list = args.searchlist

with open(search_list, 'r') as f:
    for word in f:
        word = word.rstrip('\n')
        tweets_list = []
        print(word)
        # the scraped tweets, this is a generator
        for i, tweet in enumerate(sntwitter.TwitterSearchScraper(word).get_items()):
            if i >= maxResults:
                break
            tweets_list.append([tweet.date, tweet.id, tweet.content, tweet.url, tweet.user.username, tweet.user.followersCount, tweet.replyCount, tweet.retweetCount, tweet.likeCount, tweet.quoteCount, tweet.lang,
                               tweet.outlinks, tweet.media, tweet.retweetedTweet, tweet.quotedTweet, tweet.inReplyToTweetId, tweet.inReplyToUser, tweet.mentionedUsers, tweet.coordinates, tweet.place, tweet.hashtags, tweet.cashtags])
            print(tweets_list)
            # Creating a dataframe from the tweets list above
            tweets_df = pd.DataFrame(tweets_list, columns=['Datetime', 'Tweet Id', 'Text', 'URL', 'Username', 'N. followers', 'N. replies', 'N. retweets', 'N. likes', 'N. quote', 'Language', 'Outlink', 'Media', 'Retweeted tweet', 'Quoted tweet', 'In reply to tweet ID', 'In reply to user', 'Mentioned users', 'Geo coordinates', 'Place', 'Hashtags', 'Cashtags'])
            # Export dataframe into a CSV
            tweets_df.to_csv(word + '.csv', sep='\t', index=False)
